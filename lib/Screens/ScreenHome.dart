import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

class ScreenHome extends StatefulWidget {
  @override
  _ScreenHomeState createState() => _ScreenHomeState();
}

class _ScreenHomeState extends State<ScreenHome> {
  late AudioPlayer player;
  bool isPlay = false;
  bool isStart = false;

  ElevatedButton mybutton(String _pathAsset, String _textButton) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            fixedSize: Size(MediaQuery.of(context).size.width / 4,
                MediaQuery.of(context).size.height / 15)),
        onPressed: () async {
          await player.setAsset(_pathAsset);
          player.play();
          setState(() {
            isPlay = true;
            isStart = true;
          });
        },
        child: Text(_textButton));
  }

  @override
  void initState() {
    super.initState();
    player = AudioPlayer();
    player.setLoopMode(LoopMode.all);
  }

  @override
  void dispose() {
    player.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //=====================================AppBar=============================
        appBar: AppBar(title: Text("NEPTUNE XXXIII")),
        //======END=============================AppBar====================END=====
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/neptunewp.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                  mybutton('assets/aperoSong.mp3', "APERO"),
                  mybutton('assets/repas.mp3', "REPAS"),
                  mybutton('assets/rasso.mp3', "RASSO"),
                ]))),
        floatingActionButton: new Visibility(
          visible: isStart,
          child: FloatingActionButton(
              onPressed: () {
                isPlay ? player.pause() : player.play();
                setState(() {
                  isPlay = !isPlay;
                });
              },
              child: Icon(isPlay ? Icons.pause : Icons.play_arrow)),
        ));
  }
}
